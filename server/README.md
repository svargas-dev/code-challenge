# Virtualtrips JavaScript code challenge

## NOTES ON MY SOLUTION

### Backend

Supplied data ingested into a SQLite DB (/server/models/GB.db)
NB I noticed there are duplicate locations and some non-GB locations i.e., some locations in Cyprus. In a realworld application I'd clean the data.

For the Rest API, I chose an express server and the sqlite3 for simplicity and security.
Tests are provided by jest and supertest for async requests.

[Live endpoint](https://virtualtrips-code-challenge.herokuapp.com/locations)
