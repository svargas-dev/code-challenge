"use strict";
const express = require("express");
const cors = require("cors");
const { dbQuery } = require("./models/database");
const { isEveryCharALetter } = require("./utils/utils");
const app = express();

//MIDDLEWARE - in a larger app this would get its own directory
//simple log of requests -- in a real production app it would get wrapped in a helper
app.use((req, res, next) => {
  console.log(req.method, req.path, req.ip);
  next();
});

//CORS
const corsOptions = {
  origin:
    process.env.NODE_ENV === "production"
      ? "https://sam-vargas-virtualtrips.netlify.app"
      : "http://localhost:5000",
};
//debug
console.log(corsOptions);

//ROUTES
//in a larger app these would go in a different directory e.g. routes / controllers
//app.get("/locations", cors(corsOptions), (req, res) => {
app.get("/locations", cors(corsOptions), (req, res) => {
  if (
    Object.prototype.hasOwnProperty.call(req.query, "q") &&
    req.query.q.length > 1 &&
    isEveryCharALetter(req.query.q)
  ) {
    dbQuery(
      `SELECT name, latitude, longitude from GB WHERE name LIKE ? ORDER BY name`,
      `${req.query.q}%`
    )
      .then((rows) => {
        //in production I'd wrap this in a helper function for logs
        console.log(
          "Query:",
          req.query.q,
          "DB result\n",
          rows,
          "\n",
          rows.length
        );
        res.json(rows);
      })
      .catch((err) => {
        //in production I'd wrap this in a helper function for logs
        console.error(err);
        res.json({ message: err.message });
      });
  } else {
    res.status(400).json({ message: "query not valid" });
  }
});

module.exports = app;
