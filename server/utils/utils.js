"use strict";

const isEveryCharALetter = (string) => {
  return /[a-zA-Z\s]+(%20)*$/i.test(string);
};

module.exports.isEveryCharALetter = isEveryCharALetter;
