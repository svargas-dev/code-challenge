'use strict';
const sqlite3 = require('sqlite3').verbose();

//since this isn't a full CRUD app it's safer to keep the DB as readonly
const init = (dbFilePath) => {
  return new sqlite3.Database(dbFilePath, sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      console.error(err.message);
    }

    if (!err) {
      console.log('Connected to the SQlite database.');
    }
  });
}

const dbQuery = (query, params) => {
  const db = init(__dirname + '/GB.db');
  return new Promise((resolve, reject) => {
    db.all(query, params, (err, rows) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    })
  })
  .finally(() => {
    db.close();
    console.log('DB connection closed');
  });
};

module.exports = {
  dbQuery
};