"use strict";
const { isEveryCharALetter } = require("../utils/utils");

describe("test isEveryCharALetter", () => {
  test("isEveryCharALetter 'llama'", (done) => {
    expect(isEveryCharALetter("llama")).toBe(true);
    done();
  });

  test("isEveryCharALetter 'l'", (done) => {
    expect(isEveryCharALetter("l")).toBe(true);
    done();
  });

  test("isEveryCharALetter 'abcd3'", (done) => {
    expect(isEveryCharALetter("abcd3")).toBe(false);
    done();
  });

  test("isEveryCharALetter 'jester$'", (done) => {
    expect(isEveryCharALetter("jester$")).toBe(false);
    done();
  });

  test("permit with encoded spaces", (done) => {
    expect(isEveryCharALetter("castle%20black")).toBe(true);
    done();
  });
});
