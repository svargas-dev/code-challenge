"use strict";
const app = require("../app"); // Link to your server file
const supertest = require("supertest");
const request = supertest(app);

describe("get test /locations endpoint", () => {
  test("/locations", async (done) => {
    const response = await request.get("/locations");

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("query not valid");
    done();
  });

  test("/locations?test=something invalid query", async (done) => {
    const response = await request.get("/locations?test=something");

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("query not valid");
    done();
  });

  test("/locations?q=a invalid query 1 character", async (done) => {
    const response = await request.get("/locations?q=a");

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("query not valid");
    done();
  });

  test("/locations?q=as2 invalid query including number", async (done) => {
    const response = await request.get("/locations?q=as2");

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("query not valid");
    done();
  });

  test("/locations?q=lond] invalid query including non-alphanumeric", async (done) => {
    const response = await request.get("/locations?q=lond]");

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("query not valid");
    done();
  });

  //since our data are static this is simple solution
  test("/locations?q=av valid query", async (done) => {
    const response = await request.get("/locations?q=av");

    expect(response.status).toBe(200);
    expect(response.body.length).toBe(33);
    done();
  });

  test("/locations?q=fir valid query", async (done) => {
    const response = await request.get("/locations?q=fir");

    expect(response.status).toBe(200);
    expect(response.body.length).toBe(15);
    done();
  });
});
