# Questions

Q1: Explain the output of the following code and why

```js
setTimeout(function () {
  console.log("1");
}, 100);
console.log("2");
```

```
> 2
> 1
```

A timeout is set for 100ms so console.log("2") will execute first then the unnamed function in the setTimeout callback will execute 100ms later (or thereabouts)

Q2: Explain the output of the following code and why

```js
function foo(d) {
  if (d < 10) {
    foo(d + 1);
  }
  console.log(d);
}
foo(0);
```

```
> 10
> 9
> 8
> 7
> 6
> 5
> 4
> 3
> 2
> 1
> 0
```

The function foo is defined and executed with 0 as it's argument. 0 is less than 10 and calls recursively calls itself with the most recent call reaching the console.log line first followed by the previous calls

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
```

Any falsy values will be ignored e.g. 0 or "" etc. I would prefer to use ECMAScript 2015+ synatax and use:

```js
function foo(d = 5) {
  console.log(d);
}
```

Q4: Explain the output of the following code and why

```js
function foo(a) {
  return function (b) {
    return a + b;
  };
}
var bar = foo(1);
console.log(bar(2));
```

```
> 3
```

A simple curried function. Using closures, var bar = foo(1) assigns 1 to a and bar(2) assigns 2 to b resulting in 3 (a + b).

Q5: Explain how the following function would be used

```js
function double(a, done) {
  setTimeout(function () {
    done(a * 2);
  }, 100);
}
```

Execute the done function argument with a mutliplied by 2 100ms later.
