export const isEveryCharALetter = (string) => {
  return /^[a-zA-Z\s]+$/i.test(string);
};

//Custom debounce; trailing only
export const debounce = (func, wait = 200) => {
  let timeout = null;
  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      clearTimeout(timeout);
      func(...args);
    }, wait);
  };
};
