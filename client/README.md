# Virtualtrips JavaScript code challenge

## NOTES ON MY SOLUTION

### Frontend

- Nextjs SPA with custom frontend components and propTypes for typechecking. In larger applications I'd prefer TypeScript.
- I've used CSS variables and CSS modules adhering to a modified BEM convension
- It is responsive
- It is accessbile
- Basic tests are provided by jest and react-testing-library.
- Given the nature of the site, I statically rendered the site.
- I performed additional manual tests locally with serve (see package.json), Google Lighthouse and AXE for accessbility

[Live solution](https://sam-vargas-virtualtrips.netlify.app/)
