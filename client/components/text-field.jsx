import PropTypes from "prop-types";
import clsx from "clsx";
import styles from "../styles/text-field.module.css";

export default function TextField({ value, handleChange, error }) {
  return (
    <div className={styles.textFieldWrapper}>
      <input
        data-testid="text-field-input"
        id="search"
        name="search"
        className={styles.textFieldWrapper__input}
        value={value}
        type="text"
        onChange={handleChange}
      />
      <label
        htmlFor="search"
        className={clsx(
          styles.textFieldWrapper__label,
          value && styles.textFieldWrapper__label___mini
        )}
      >
        Search GB Locations
      </label>
      <span
        className={styles.textFieldWrapper__error}
        style={{ visibility: error ? "visible" : "hidden" }}
      >
        Only letters are allowed
      </span>
    </div>
  );
}

TextField.propTypes = {
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  error: PropTypes.bool,
};
