import { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import styles from "../styles/map-modal.module.css";

export const latitudeValidation = /^-*\d{1,2}.\d+$/; //this validation is still quite basic -it would need to test a max of 90 min -90
export const longitudeValidation = /^-*\d{1,3}.\d+$/; //this validation is still quite basic -it would need to test a max of 180 min -180

export default function MapModal({
  name,
  latitude,
  longitude,
  handleCloseMapModal,
}) {
  const wrapperRef = useRef(null);

  useEffect(() => {
    wrapperRef.current.focus();
  }, [wrapperRef]);

  return (
    <div
      id="map-modal--wrapper"
      ref={wrapperRef}
      //accessibility
      tabIndex="-1"
      role="dialog"
      className={styles.mapModal__wrapper}
      style={{
        top: window.scrollY,
      }}
    >
      <div className={styles.mapModal__bg} onClick={handleCloseMapModal}></div>

      <div data-testid="map-modal" className={styles.mapModal__content}>
        <button className={styles.btn___close} onClick={handleCloseMapModal}>
          Close
        </button>
        <span className={styles.mapModal__place}>{name}</span>
        <p>
          Latitude: {latitude}
          &ensp;Longitude: {longitude}
        </p>
        <iframe
          title={name + " (Open Street Map)"}
          width={window.innerWidth > 450 ? "425" : window.innerWidth * 0.8}
          height="350"
          frameBorder="0"
          scrolling="no"
          marginHeight="0"
          marginWidth="0"
          src={`https://www.openstreetmap.org/export/embed.html?bbox=${
            +longitude + 0.03
          }%2C${latitude - 0.01}%2C${+longitude - 0.03}%2C${
            +latitude + 0.01
          }&amp;layer=mapnik`}
          style={{ border: "1px solid black" }}
        ></iframe>
        <br />
        <small>
          <a
            href={`https://www.openstreetmap.org/#map=17/${latitude}-${longitude}`}
          >
            View Larger Map
          </a>
        </small>
      </div>
    </div>
  );
}

MapModal.propTypes = {
  name: PropTypes.string.isRequired,
  latitude: (props, latitude, MapModal) => {
    if (!latitudeValidation.test(props[latitude])) {
      return new Error(
        "Invalid prop `" +
          latitude +
          "` supplied to" +
          " `" +
          MapModal +
          "`. Validation failed."
      );
    }
  },
  latitude: (props, longitude, MapModal) => {
    if (!longitudeValidation.test(props[longitude])) {
      return new Error(
        "Invalid prop `" +
          longitude +
          "` supplied to" +
          " `" +
          MapModal +
          "`. Validation failed."
      );
    }
  },
  handleCloseMapModal: PropTypes.func.isRequired,
};
