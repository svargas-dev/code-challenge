//I could have used the normalize npm package (or the Material UI CSS baseline
//or created a manual CSS reset) but I chose this method for simplicity for this challenge
import "../styles/normalize.css";
import "../styles/variables.css";
import "../styles/global.css";

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
