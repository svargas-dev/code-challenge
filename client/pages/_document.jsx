import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="en-gb">
        <Head>
          <link rel="shortcut icon" href="/favicon.ico" key="favicon" />
          <link rel="apple-touch-icon" href="/logo192.png" key="apple-icon" />
          <link
            rel="preconnect"
            href="https://fonts.gstatic.com"
            crossOrigin="anonymous"
            key="google-fonts"
          />
          <link
            rel="preload"
            as="style"
            href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&amp;display=swap"
            key="google-lato-rochester"
          />
          <meta name="author" content="Sam Vargas" />
          <meta
            name="description"
            content="Virtualtrips full-stack coding challenge. Search locations in the UK and show on a map along with coordinates"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
