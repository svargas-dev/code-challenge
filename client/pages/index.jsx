import { useState, useEffect } from "react";
import { debounce } from "../utils";
import Head from "next/head";
import { isEveryCharALetter } from "../utils/";
import TextField from "../components/text-field";
import MapModal from "../components/map-modal";
import styles from "../styles/index.module.css";

const isProduction = process.env.NODE_ENV === "production";

export default function Home() {
  const [value, setValue] = useState("");
  const [error, setError] = useState(false);
  const [data, setData] = useState(null);
  const [place, setPlace] = useState(null);
  const [lastFocus, setLastFocus] = useState(null);

  const handleSeeMap = (place) => {
    setPlace(place);
    document.body.classList.add("stop-scrolling");
    //accessibility
    setLastFocus(document.activeElement);
  };

  const handleCloseMapModal = () => {
    document.body.classList.remove("stop-scrolling");
    setPlace(null);
    lastFocus.focus();
  };

  useEffect(() => {
    function escapeListener() {
      window.addEventListener("keydown", (e) => {
        if (place && e.code === "Escape") {
          handleCloseMapModal();
        }
      });
    }
    escapeListener();

    return escapeListener();
  }, [place]);

  const fetchAndSetData = async (query) => {
    const url = `${
      isProduction
        ? "https://virtualtrips-code-challenge.herokuapp.com/"
        : "http://localhost:3000/"
    }locations?q=${query.toLowerCase()}`;

    try {
      const res = await fetch(url, {
        mode: "cors",
        headers: {
          accept: "application/json",
        },
      });
      //console.log("res:", res);
      let data;
      if (res) data = await res.json();
      //console.log("data:", data);
      if (data) setData(data);
    } catch (e) {
      console.error("Error fetching from API\n", e);
    }
  };
  const fetchAndSetDataDebounced = debounce(fetchAndSetData);

  const handleChange = (event) => {
    setValue(event.target.value);

    if (event.target.value && !isEveryCharALetter(event.target.value)) {
      setError(true);
      return;
    }

    if (event.target.value.length < 2) {
      setData(null);
      return;
    }

    if (
      error &&
      (!event.target.value || isEveryCharALetter(event.target.value))
    ) {
      setError(false);
    }

    if (
      !error &&
      event.target.value.length > 1 &&
      isEveryCharALetter(event.target.value)
    ) {
      //copy the value
      const query = event.target.value;
      fetchAndSetDataDebounced(query);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  return (
    <div className="container" aria-hidden={place ? true : false}>
      <Head>
        <title>Sam Vargas - Virtualtrips Code Challenge</title>
      </Head>

      <header className={styles.header}>
        <h1 className={styles.header__title}>Sam Vargas</h1>
        <div className={styles.header__subTitleWrapper}>
          {/* In a larger app I'd probably use something like @svgr/webpack (or next.js Image if SSR) so the SVG is inlined*/}
          <img
            src="virtualtrips.svg"
            alt="Virtual Trips"
            width="160"
            height="31"
          />
          <span className={styles.header__subTitle}>Code Challenge</span>
        </div>
      </header>

      <main className={styles.main}>
        <form onSubmit={handleSubmit}>
          <TextField value={value} handleChange={handleChange} error={error} />
        </form>

        {data && data.length > 0 && (
          <table className={styles.table}>
            <tbody>
              {data.map((place, index) => (
                //There are duplicate data in the set so we need the index in addition to name
                //e.g. Larkesbeare House
                <tr key={`${place.name}${index}`}>
                  <td className={styles.col1}>{place.name}</td>
                  <td className={styles.col2}>
                    <button
                      className={styles.mapLink}
                      onClick={() => handleSeeMap(place)}
                    >
                      see on map
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </main>

      {place && (
        <MapModal {...place} handleCloseMapModal={handleCloseMapModal} />
      )}
    </div>
  );
}
