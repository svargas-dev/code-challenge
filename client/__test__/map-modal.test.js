import { cleanup, render, screen } from "@testing-library/react";
import MapModal, {
  latitudeValidation,
  longitudeValidation,
} from "../components/map-modal";

afterEach(cleanup);

describe("MapModal", () => {
  test("mounts without error", () => {
    render(
      <MapModal
        name="test"
        latitude="51.4898"
        longitude="-0.0882"
        handleCloseMapModal={() => console.log("handleChange test")}
      />
    );

    expect(screen.getByTestId("map-modal")).toHaveTextContent("test");
    expect(screen.getByTestId("map-modal")).toHaveTextContent("51.4898");
    expect(screen.getByTestId("map-modal")).toHaveTextContent("-0.0882");
  });

  test("latitude validation", () => {
    expect(latitudeValidation.test("41.48800")).toBeTruthy();
    expect(latitudeValidation.test("-41.48800")).toBeTruthy();
    expect(latitudeValidation.test("51.4898z")).toBeFalsy();
  });

  test("longitude validation", () => {
    expect(longitudeValidation.test("151.48800")).toBeTruthy();
    expect(longitudeValidation.test("-151.48800")).toBeTruthy();
    expect(longitudeValidation.test("51.4898z")).toBeFalsy();
  });
});
