import { isEveryCharALetter } from "../utils";

describe("test isEveryCharALetter", () => {
  test("isEveryCharALetter 'llama'", (done) => {
    expect(isEveryCharALetter("llama")).toBe(true);
    done();
  });

  test("isEveryCharALetter 'l'", (done) => {
    expect(isEveryCharALetter("l")).toBe(true);
    done();
  });

  test("isEveryCharALetter 'abcd3'", (done) => {
    expect(isEveryCharALetter("abcd3")).toBe(false);
    done();
  });

  test("isEveryCharALetter 'jester$'", (done) => {
    expect(isEveryCharALetter("jester$")).toBe(false);
    done();
  });

  test("permit spaces", (done) => {
    expect(isEveryCharALetter("castle black")).toBe(true);
    done();
  })
});
