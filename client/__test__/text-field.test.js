import { render, screen } from "@testing-library/react";
import TextField from "../components/text-field";

describe("TextField", () => {
  test("mounts without error", () => {
    render(
      <TextField
        value="test"
        handleChange={() => console.log("handleChange test")}
      />
    );

    expect(screen.getByTestId("text-field-input").value).toBe("test");
    expect(screen.getByTestId("text-field-input").value).not.toBe("");
  });
});
